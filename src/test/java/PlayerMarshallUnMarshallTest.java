

import java.io.IOException;
import java.util.TimeZone;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import sun.jvm.hotspot.utilities.Assert;

@RunWith(SpringRunner.class)
@Import(PlayerMarshallUnMarshallTest.TestConfiguration.class)
public class PlayerMarshallUnMarshallTest {

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void test_player_serialization() throws IOException {
        final Player remco = Player.builder()
                .firstName("Remco")
                .lastName("Ruijsenaars")
                .build();

        String remcoAsJson = objectMapper.writeValueAsString(remco);
        final Player remcoAgain = objectMapper.readValue(remcoAsJson, Player.class);

        Assert.that(remco.equals(remcoAgain), "lucky me" );
    }

    @Configuration
    public static class TestConfiguration{

        @Bean
        public ObjectMapper getObjectMapper(){
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setTimeZone(TimeZone.getDefault());
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            return objectMapper;
        }
    }


}